# Water Drone
This repo contains the code for the onboard computer on the water drone. The project is aimed to log GNSS position and time and sonar data. The raspberry pi (onboard computer) will used as a hotspot to host grafana as a dashboard to display the data. Furthermore, there will be the option to configure the sonar to the specific requirements needed for the surveying. 

## File Structure

```plaintext
- water_drone/
  - src/
    - main.py
    - utils/
      - __init__.py
      - log.py
  - tests/
  - scripts/
  - README.md
  - requirements.txt
  - .gitignore
  ```

  ## Install requirements
Run the following line in the terminal to install dependencies \
`pip install -r requirements.txt`

## Logins
### Hotspot
SSID: dtuspace_rpi
Pass: dtuspace

### SSH
SSID: 192.168.0.10
Pass: chocolate island laboratory

### Grafana
SSID: admin@localhost
Pass: foreigner appointment story husband
IP: 192.168.0.10:3000
Data:192.168.0.10/data
