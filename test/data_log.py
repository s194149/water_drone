import os
import serial
import time
import csv

# Create a serial object
ser = serial.Serial()

# Define the port and baudrate
ser.port = 'COM4'
ser.baudrate = 115200

# Open the port
ser.open()

# Create a buffer for the incoming data
buffer = ''

# Specify the subfolder and filename for the output
folder = 'test_data/'
filename = 'output.csv'
os.makedirs(folder, exist_ok=True)  # Creates the folder if it doesn't exist
filepath = os.path.join(folder, filename)  # Joins the folder name and file name

# Open a CSV file for writing
with open(filepath, 'w', newline='', encoding='utf-8') as file:
    writer = csv.writer(file)

    # Check if the port is open
    if ser.is_open:
        print("Successfully opened port", ser.name)
        start_time = time.time()
        while True:
            time.sleep(0.01)
            # Read data from the UART port
            while ser.in_waiting:
                buffer += ser.read(ser.in_waiting).decode('utf-8', 'ignore')

            # If the buffer contains at least one complete message, process it
            if '\n' in buffer:
                message, buffer = buffer.split('\n', 1)
                
                # Write the message to the CSV file
                writer.writerow([message])
                
                # If the message contains "##DataEnd", stop reading and break the loop
                if "##DataEnd" in message:
                    break
        end_time = time.time()
        print(f"Time taken to write to the file: {end_time - start_time} seconds")

    else:
        print("Failed to open port", ser.name)

# Close the port
ser.close()
